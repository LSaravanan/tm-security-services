﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TMService.API.Services
{
    public interface ILoggingService
    {
        void Log(string message, string exceptionDetails, string loglevel);
    }
}
