﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TM.Domain.Logic;
using TM.Domain.Logic.DomainModel;
using TM.Domain.Logic.Repository;

namespace TMService.API.Controllers
{
   
    [Route("api/user")]
    public class UserController : Controller
    {
        private IUnitOfWork _unitofwork;
        private JwtSettings _settings;

        public UserController(IUnitOfWork unitofwork, JwtSettings settings)
        {
            _unitofwork = unitofwork;
            _settings = settings;
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody]User user)
        {
            
            AppUserAuth auth = new AppUserAuth();
            SecurityManager mgr = new SecurityManager(_unitofwork, _settings);

            auth = mgr.ValidateUser(user);
            if (auth.IsAuthenticated)
            {
                return Ok(auth);
            }
            else
            {
                return NotFound("Invalid user Name/Password.");
            }
        }
    }
}
