#Build Stage
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 as build-env

WORKDIR /tmsecurityservice

COPY . .

RUN dotnet restore

RUN dotnet publish -o /publish


# Runtime Stage
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /publish
COPY --from=build-env /publish /publish
ENTRYPOINT [ "dotnet", "TMService.API.dll" ]