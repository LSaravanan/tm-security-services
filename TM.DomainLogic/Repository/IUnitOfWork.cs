﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.Repository
{
    public interface IUnitOfWork
    {
        IUserRepository users { get; }
        void Commit();
    }
}
