﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.Repository
{
    public class SqlUnitOfWork : IUnitOfWork
    {
        private DomainContext _context;
        private IUserRepository _userRepository;


        public SqlUnitOfWork(DomainContext context)
        {
            _context = context;
        }

        public IUserRepository users
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_context);
                }
                return _userRepository;
            }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
