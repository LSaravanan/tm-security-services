﻿using System;
using System.Collections.Generic;
using System.Text;
using TM.Domain.Logic.DomainModel;

namespace TM.Domain.Logic.Repository
{
    public interface IUserRepository
    {
        User Getuser(User user);

        List<UserClaim> GetUserClaims(int id);

    }
}
