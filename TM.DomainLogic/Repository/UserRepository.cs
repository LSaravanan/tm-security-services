﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TM.Domain.Logic.DomainModel;

namespace TM.Domain.Logic.Repository
{
    public class UserRepository : IUserRepository
    {
        private DomainContext _context;

        public UserRepository(DomainContext context)
        {
            _context = context;
        }

        public User Getuser(User user)
        {
           return  _context.Users
                 .Where(u => u.userName.ToLower() == user.userName.ToLower() && u.password == user.password)
                 .FirstOrDefault();
        }

        public List<UserClaim> GetUserClaims(int id)
        {
            return _context.UserClaims.Where(u => u.userid == id).ToList();
        }
    }
}
