﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TM.Domain.Logic.DomainModel;

namespace TM.Domain.Logic
{
    public class DomainContext:DbContext
    {

        public DomainContext(DbContextOptions<DomainContext> options)
            :base(options)
        {
            
        }

        public DbSet<User> Users { get; set; }


        public DbSet<UserClaim> UserClaims { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            
        }
    }
}
