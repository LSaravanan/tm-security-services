﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TM.Domain.Logic.Repository;

namespace TM.Domain.Logic.DomainModel
{
    public class SecurityManager
    {
        private IUnitOfWork _unitofwork;
        private JwtSettings _settings;

        public SecurityManager(IUnitOfWork unitofwork, JwtSettings settings)
        {
            _unitofwork = unitofwork;
            _settings = settings;
        }

        public AppUserAuth ValidateUser(User user)
        {
            AppUserAuth ret = new AppUserAuth();
            User authUser = null;

            authUser = _unitofwork.users.Getuser(user);

            if(authUser !=null)
            {
                ret = BuildUserAuthObject(authUser);
            }

            return ret;
        }

        protected AppUserAuth BuildUserAuthObject(User user)
        {
            AppUserAuth ret = new AppUserAuth();

            ret.Id = user.Id;
            ret.UserName = user.userName;
            ret.IsAuthenticated = true;
            ret.BearerToken = new Guid().ToString();

            ret.Claims = _unitofwork.users.GetUserClaims(user.Id);

           
            ret.BearerToken = BuildJwtToken(ret);

            return ret;

        }

        protected string BuildJwtToken(AppUserAuth authUser)
        {
            SymmetricSecurityKey key = new SymmetricSecurityKey(
              Encoding.UTF8.GetBytes(_settings.Key));

            // Create standard JWT claims
            List<Claim> jwtClaims = new List<Claim>();
            jwtClaims.Add(new Claim(JwtRegisteredClaimNames.Sub,
                authUser.UserName));
            jwtClaims.Add(new Claim(JwtRegisteredClaimNames.Jti,
                Guid.NewGuid().ToString()));

            foreach(var claim in authUser.Claims)
            {
                jwtClaims.Add(new Claim(claim.ClaimType, claim.ClaimValue));
            }
            

            // Create the JwtSecurityToken object
            var token = new JwtSecurityToken(
              issuer: _settings.Issuer,
              audience: _settings.Audience,
              claims: jwtClaims,
              notBefore: DateTime.UtcNow,
              expires: DateTime.UtcNow.AddMinutes(
                  _settings.MinutesToExpiration),
              signingCredentials: new SigningCredentials(key,
                          SecurityAlgorithms.HmacSha256)
            );

            // Create a string representation of the Jwt token
            return new JwtSecurityTokenHandler().WriteToken(token); ;
        }
    }
}
