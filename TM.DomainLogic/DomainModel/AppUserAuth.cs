﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.DomainModel
{
    public class AppUserAuth
    {
        public AppUserAuth() : base()
        {
            UserName = "Not authorized";
            BearerToken = string.Empty;
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string BearerToken { get; set; }
        public bool IsAuthenticated { get; set; }

        public List<UserClaim> Claims { get; set; }

        public int role { get; set; }


    }
}
