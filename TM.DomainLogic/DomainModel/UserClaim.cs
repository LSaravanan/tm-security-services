﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TM.Domain.Logic.DomainModel
{
    public class UserClaim : Entity
    {

        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }

        public int userid { get; set; }
    }
}
